﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketLauncher : MonoBehaviour, I_Weapon
{
    public Transform shotOrigin;
    public GameObject projectile;
    AttackController controller;
    GameObject mouse;
    public int maxAmmo = 5, currentAmmo;
    public WeaponUI ui;
    public float shotTimer = 1;
    public bool canShoot = true;
    float shotTimerMax;
    GameObject shot;

    private void OnEnable()
    {
        controller = GetComponentInParent<AttackController>();
        controller.ChangeWeapon(this.gameObject);

    }


    public void Start()
    {
        mouse = GameObject.FindWithTag("Mouse");
        currentAmmo = maxAmmo;
        PlayerController.landEvent += RefillAmmo;
        shotTimerMax = shotTimer;

    }

    private void Update()
    {
        if (shotTimer < shotTimerMax)
        {
            shotTimer += Time.deltaTime;
        }
        else if (shotTimer > shotTimerMax)
        {
            shotTimer = shotTimerMax;
        }

        if (Input.GetMouseButtonDown(1))
            if (shot != null)
                DetonateShot();

    }

    public void Attack()
    {
        if (currentAmmo > 0 && shotTimer == shotTimerMax)
        {
            shot = Instantiate(projectile, shotOrigin.position, Quaternion.identity);
            currentAmmo--;
            ui.UpdateAmmo(currentAmmo, maxAmmo);
            shotTimer = 0;
        }


    }

    public void DetonateShot()
    {
        if(shot != null)
            shot.GetComponent<MissileBehaviour>().Detonate();
        shot = null;
    }

    public int ReturnAmmo()
    {
        return currentAmmo;
    }

    public void RefillAmmo()
    {
        currentAmmo = maxAmmo;
        ui.UpdateAmmo(currentAmmo, maxAmmo);
    }
}
