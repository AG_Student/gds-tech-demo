﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{

    CircleCollider2D col;
    public float minForce, forceIncreaseMultiplier, maxRadius, radiusMultiplier;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localScale.x < maxRadius)
        {
            transform.localScale += (new Vector3(transform.localScale.x * radiusMultiplier, transform.localScale.y  * radiusMultiplier, transform.localScale.z));
            minForce -= Time.deltaTime * forceIncreaseMultiplier;
        }
        else
            Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.layer == 10)
        {
            collision.gameObject.GetComponent<I_Damageable>().Damage((int)minForce);
        }
        else if(collision.gameObject.tag == "Player")
        {
            Vector2 forceDir = (collision.gameObject.transform.position - transform.position);
            forceDir = forceDir.normalized;
            PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
            player.rb.velocity = new Vector2(player.rb.velocity.x, 0f);
            player.ApplyForce(forceDir * minForce);

        }
    }
}
