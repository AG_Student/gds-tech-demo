﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponUI : MonoBehaviour
{
    Image weaponImage;
    public Text weaponName, ammoRemaining;

    public void ApplyWeapon(GameObject weapon)
    {
        I_Weapon script = weapon.GetComponent<I_Weapon>();
        weaponName.text = weapon.gameObject.name;
        ammoRemaining.text = script.ReturnAmmo().ToString() + " / " + script.ReturnAmmo().ToString();
    }

    public void UpdateAmmo(int current, int max)
    {
        ammoRemaining.text = current.ToString() + " / " + max.ToString();
    }
}
