﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponManager : MonoBehaviour
{
    AttackController player;
    public GameObject[] weapons = new GameObject[2];

    public WeaponUI uiScript;
    Image weaponImage;


    //int currentWeapon = 1;

    private void Start()
    {
        player = GetComponent<AttackController>();

    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
            EnableWeapon(0);
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            EnableWeapon(1);
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            EnableWeapon(2);

    }
    public void EnableWeapon(int weapon)
    {
        for (int i = 0; i < weapons.Length; i++)
        {
            if (i == weapon)
                weapons[i].SetActive(true);
            else
                weapons[i].SetActive(false);
        }
        player.ChangeWeapon(weapons[weapon]);
        ChangeWeaponUI(weapons[weapon]);
    }

    public void ChangeWeaponUI(GameObject weapon)
    {
        uiScript.ApplyWeapon(weapon);
    }


}
