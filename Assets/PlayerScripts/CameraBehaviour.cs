﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    //Script for controlling camera behaviour, e.g. follow the player, delayed follow, pan, zoom, change angle

    public Transform targetToFollow; //target to follow
    public Vector3 cameraOffset;

    private void Start()
    {
        
    }

    private void LateUpdate()
    {
        transform.position = new Vector3(targetToFollow.position.x, targetToFollow.position.y, targetToFollow.position.z) + cameraOffset; 

    }
}
