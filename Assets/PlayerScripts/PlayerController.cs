﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour, I_Damageable
{

    [Header("Horizontal Movement")]
    public float moveSpeed = 10f;
    public float oldmoveSpeed;
    private float horizontalMovement;
    public Vector2 direction;

    [Header("Vertical Movement")]
    public float jumpSpeed = 15f;
    public float jumpDelay = 0.25f;
    private float jumpTimer;


    [Header("Components")]
    public Rigidbody2D rb;
    public LayerMask groundLayer;

    [Header("Physics")]
    public float maxSpeed = 7f;
    public float oldMaxSpeed;
    public float linearDrag = 4f;
    public float gravity = 1;
    public float fallMultiplier = 5;
    public float jumpHeight = 5f;

    [Header("Air Dodge")]
    public float dodgeForce;
    public float dodgeTimeMax;
    float dodgeTime;
    public bool canDodge = true;

    [Header("Collision")]
    public bool OnGround = false;
    public float groundLength = 0.6f;
    public Vector3 colliderOffset;

    [Header("sound effects")]
    public AudioClip smallJump;
    public AudioClip bigJump;

    [Header("check")]
    public bool follow;

    [Header("Animator")]
    private Animator anim;

    [Header("Health")]
    public GameObject healthBar;

    PlayerPhysicsController physicsController;
    WeaponManager weaponManager;

    public delegate void LandEvent();
    public static event LandEvent landEvent;
    bool isDodging = false;

    // Start is called before the first frame update
    void Start()
    {
        weaponManager = GetComponent<WeaponManager>();
        physicsController = GetComponent<PlayerPhysicsController>();
        rb = GetComponent<Rigidbody2D>(); 
       // anim = gameObject.GetComponent<Animator>();
        oldMaxSpeed = maxSpeed;
        oldmoveSpeed = moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        PlayerInput();

        OnGround = Physics2D.Raycast(transform.position + colliderOffset, Vector2.down, groundLength, groundLayer) || Physics2D.Raycast(transform.position - colliderOffset, Vector2.down, groundLength, groundLayer);
        if (OnGround)
        {
            canDodge = true;
            landEvent();
            
        }

       ForwardInputCheck();
        JumpAnim();
        Death();

    }

    private void FixedUpdate()
    {
        MoveCharacter(direction.x);

        if (jumpTimer > Time.time && OnGround)
        {
            Jump();
        }

        
        ModifyPhysics();
        PreventSlide();
    }

    public void ForwardInputCheck()
    {
        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            
            follow = true;
        }
        else
        {
            follow = false;
        }
    }

    // gets player input for jumping and moving, sets the jump timer to allow for hangtime
    void PlayerInput()
    {
        direction = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        horizontalMovement = rb.velocity.x;
        //anim.SetFloat("Speed", Mathf.Abs(horizontalMovement));

        if (Input.GetKeyDown(KeyCode.D))
        {
            RotateRight();
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            RotateLeft();
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            jumpTimer = Time.time + jumpDelay;
            
        }

        if(Input.GetKeyDown(KeyCode.Space) && !OnGround)
        {
            if (canDodge)
            {
                AirDodge(direction);
                StartCoroutine(AirDodgeTimer());
            }
        }
        

        //if (Input.GetKeyDown(KeyCode.LeftShift))
        //{
        //    moveSpeed = moveSpeed * 2;
        //    maxSpeed = maxSpeed * 2;
        //    Debug.Log(moveSpeed);
        //}
        //else if (Input.GetKeyUp(KeyCode.LeftShift))
        //{
        //    moveSpeed = oldmoveSpeed;
        //    maxSpeed = oldMaxSpeed;
        //    Debug.Log(moveSpeed);
        //}
    }

    IEnumerator AirDodgeTimer()
    {
        Vector2 velocityTemp = rb.velocity;
        isDodging = true;
        //canDodge = false;
        float gravityTemp = rb.gravityScale;
        rb.gravityScale = 0f;
        while(dodgeTime > 0f)
        {
            dodgeTime -= Time.deltaTime;
            yield return null;
        }
        dodgeTime = dodgeTimeMax;
        isDodging = false;
       // canDodge = true;
        rb.gravityScale = gravityTemp;
        yield return null;
    }

    void AirDodge(Vector3 dir)
    {
        rb.velocity = Vector2.zero;
        if (dir.y == 0)
        {
            dir.y = .3f;
            dir.x = dir.x * 1.3f;
        }
        rb.AddForce(dir * dodgeForce, ForceMode2D.Impulse);
        canDodge = false;
    }

    void RotateLeft()
    {
        transform.eulerAngles = new Vector3(0, 180, 0);
    }


    void RotateRight()
    {
        transform.eulerAngles = new Vector3(0, 0, 0);
    }

    void JumpAnim()
    {
       
        if (Input.GetKey(KeyCode.Space) || OnGround == false)
        {
            
            //anim.SetBool("Jumping", true);
        }
        else
        {
         //   anim.SetBool("Jumping", false);
        }
    }

    // character movement physocs
    void MoveCharacter(float horizontal)
    {
        rb.AddForce(Vector2.right * horizontal * moveSpeed);

        if (Mathf.Abs(rb.velocity.x) > maxSpeed)
        {
            rb.velocity = new Vector2(Mathf.Sign(rb.velocity.x) * maxSpeed, rb.velocity.y);
        }

        //rb.velocity = new Vector2(horizontal * moveSpeed, rb.velocity.y);

    }

    // jump function
    void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
        rb.AddForce(Vector2.up * jumpSpeed, ForceMode2D.Impulse);
        jumpTimer = 0;


    }

    public void JumpReset()
    {
        rb.velocity = new Vector2(rb.velocity.x, 0);
    }

    // physics modifiers to make things feel better
    void ModifyPhysics()
    {
        bool changeDirections = (direction.x > 0 && rb.velocity.x < 0) || (direction.x < 0 && rb.velocity.x > 0);


        if (OnGround)
        {
            if (Mathf.Abs(direction.x) < 0.4f || changeDirections)
            {
               // rb.drag = linearDrag;
            }
            else
            {
               // rb.drag = 0f;
            }
            //rb.gravityScale = 0;

        }
        else
        {
            // gravity manipulation based on of the player is holding down space, allows for the variable jump
            rb.gravityScale = gravity;
            rb.drag = linearDrag * 0.15f;

            if (rb.velocity.y < 0)
            {
                //rb.gravityScale = gravity * fallMultiplier;
            }
            else if (rb.velocity.y > 0 && !Input.GetKey(KeyCode.Space))
            {
                rb.gravityScale = gravity * fallMultiplier;
            }

        }
    }


    public void Death()
    {
        if (healthBar.GetComponent<HealthBar>().currentHP <= 0)
        {
            this.GetComponent<Respawn>().respawn();
            healthBar.GetComponent<HealthBar>().currentHP = healthBar.GetComponent<HealthBar>().maxHP;
            //Destroy(this.gameObject);
        }
    }

    public void LifeMan()
    {
        gameObject.GetComponent<Collider2D>().enabled = true;
    }

    //debug lines
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position + colliderOffset, transform.position + colliderOffset + Vector3.down * groundLength);
        Gizmos.DrawLine(transform.position - colliderOffset, transform.position - colliderOffset + Vector3.down * groundLength);
    }

    public void Damage(int damage)
    {
        healthBar.GetComponent<HealthBar>().currentHP -= damage;
        // Destroy(this.gameObject);
        Debug.Log("hit");
        ComboManager.Instance.StopCombo();
    }

    void PreventSlide()
    {
        if (OnGround)
        {
            if(direction.x == 0)
            {
                rb.velocity = new Vector2(0, rb.velocity.y);
            }
        }
    }

    public void DodgeReset()
    {
        if (!canDodge)
            canDodge = true;
    }

}

