﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlowTimeController : MonoBehaviour
{
    //Script for controlling slowing time, both through player input and other game actions

   // UIManager UImanager; //link to manager 
    float slowTimeAmount = .15f, slowTimeResource = 10f, slowTimeMax = 10f; //resources
    bool slowingTime = false;

    //Called before Start
    private void Awake()
    {
       // UImanager = GameObject.FindGameObjectWithTag("Manager").GetComponent<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {
        HandleInput();
        if (slowingTime)
            ReduceGauge();
    }

    //Receives input from the player
    private void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            Slowdown();
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            if (slowingTime)
            {
                StartCoroutine("SpeedUp");
                StartCoroutine("RestoreGauge");
            }
        }
    }

    //reduces the gauge and updates the UI
    private void ReduceGauge()
    {
        slowTimeResource -= Time.deltaTime * 2;
        if (slowTimeResource <= 0)
        {
            StartCoroutine("SpeedUp");
            StartCoroutine("RestoreGauge");
        }
       // UImanager.UpdateSlowMotionGauge(slowTimeResource);
    }

    //slows time
    public void Slowdown()
    {
        if (!slowingTime)
        {
            Time.timeScale = slowTimeAmount;
            // Time.fixedDeltaTime = (Time.timeScale * .02f) * 2; //for physics
            Time.fixedDeltaTime = Time.timeScale * .02f;
            slowingTime = true;
        }

    }



    //begins returning time to normal
    IEnumerator SpeedUp()
    {

        slowingTime = false;
        while (Time.timeScale < 1f)
        {
            if (slowingTime)
            {
                yield break; //cancels the coroutine if the player starts slowing time again
            }
            Time.timeScale += Time.deltaTime * 2;
            //   Time.fixedDeltaTime = (Time.timeScale * .02f) * 2;
            Time.fixedDeltaTime = Time.timeScale * .02f;

            yield return null;
        }
        //  Time.fixedDeltaTime = .02f;
    }

    //restores the gauge and updates the UI
    IEnumerator RestoreGauge()
    {
        while (!slowingTime && slowTimeResource <= slowTimeMax)
        {
            slowTimeResource += Time.deltaTime / 2;
          //  UImanager.UpdateSlowMotionGauge(slowTimeResource);
            yield return null;
        }
    }
}
