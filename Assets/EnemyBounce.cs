﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBounce : MonoBehaviour
{
    I_Damageable enemy;
    public float bounceForce;

    private void Start()
    {
        enemy = GetComponent<I_Damageable>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.tag == "Player" && collision.gameObject.layer != 13)
        {
            PlayerPhysicsController player = collision.gameObject.GetComponent<PlayerPhysicsController>();
            player.rb.velocity = Vector2.zero;
            player.ApplyForce((player.transform.position - transform.position) * bounceForce);
            enemy.Damage(10);
        }
    }
}
