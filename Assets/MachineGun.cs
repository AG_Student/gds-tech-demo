﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGun : MonoBehaviour, I_Weapon
{
    public GameObject shot;
    public Transform shotOrigin, mouse;
    public PlayerPhysicsController player;
    public int maxAmmo, currentAmmo;
    public WeaponUI ui;
    public float shotTimer = 1;
    public bool canShoot = true;
    float shotTimerMax;

    // Start is called before the first frame update
    void Start()
    {
        shotTimerMax = shotTimer;
        currentAmmo = maxAmmo;
        mouse = GameObject.FindWithTag("Mouse").transform;
        PlayerController.landEvent += RefillAmmo;
    }

    private void Update()
    {
        if (shotTimer < shotTimerMax)
        {
            shotTimer += Time.deltaTime;
        }
        else if (shotTimer > shotTimerMax)
        {
            shotTimer = shotTimerMax;
        }
        else if (Input.GetMouseButtonUp(0))
            shotTimer = shotTimerMax;


    }

    public void Attack()
    {
        if (currentAmmo > 0 && shotTimer == shotTimerMax)
        {
            shotTimer = 0;
            player.rb.velocity = Vector3.zero;
            player.ApplyForce(-(mouse.position - shotOrigin.position) / 3);
            MachineGunBullet bullet = Instantiate(shot, shotOrigin.position, Quaternion.identity).GetComponent<MachineGunBullet>();
            bullet.SetTrajectory(mouse.position - shotOrigin.position);
            currentAmmo--;
            ui.UpdateAmmo(currentAmmo, maxAmmo);
        }
    }

    public int ReturnAmmo()
    {
        return currentAmmo;
    }
    public void RefillAmmo()
    {
        currentAmmo = maxAmmo;
        ui.UpdateAmmo(currentAmmo, maxAmmo);
    }
}
