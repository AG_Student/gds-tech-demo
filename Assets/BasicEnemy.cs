﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : MonoBehaviour, I_Damageable
{

    GameObject player;
    public GameObject shot, shotOrigin;
    public Vector2 moveVector;
    public float moveSpeed, attackRange, attackTimer;
    public int health;
    Rigidbody2D rb;
    float attackTimerMax;
    public bool canMove = false, canShoot = false;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        rb = GetComponent<Rigidbody2D>();
        attackTimerMax = attackTimer;

    }

    // Update is called once per frame
    void Update()
    {
        GetTarget();
    }
    void GetTarget()
    {
        moveVector = player.transform.position - transform.position;
        moveVector.y = 0;
    }
    private void FixedUpdate()
    {
        if (CheckAttackDistance() == true)
        {
            if(canShoot)
            Attack();
        }
        
        if(canMove)
            Move(moveVector);
    }
    void Move(Vector2 dir)
    {
        dir = dir.normalized;
        dir = dir * moveSpeed ;
        rb.velocity = dir;
    }
    bool CheckAttackDistance()
    {
        float distance = Vector2.Distance(player.transform.position, transform.position);
        
        if (distance <= attackRange)
        {
            return true;
        }
        else
            return false;
    }
    void Attack()
    {
        if (attackTimer <= 0f)
        {
            GameObject bullet = Instantiate(shot, shotOrigin.transform.position, Quaternion.identity);
            bullet.GetComponent<MachineGunBullet>().SetTrajectory(player.transform.position - shotOrigin.transform.position);
            attackTimer = attackTimerMax;
        }
        else
        {
            attackTimer -= Time.deltaTime;
        }
    }
    public void Damage(int damage)
    {
        ComboManager.Instance.CallCombo(30f);
        Destroy(this.gameObject);
        player.GetComponent<PlayerController>().DodgeReset();
    }
}
