﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleHook : MissileBehaviour
{
    public List<int> targetLayers;
    GameObject player;
    PlayerPhysicsController playerController;
    public bool attached = false;
    public float grappleForce = 5;

    private new void Start()
    {
        base.Start();
        player = GameObject.FindWithTag("Player");
        playerController = player.GetComponent<PlayerPhysicsController>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (targetLayers.Contains(collision.gameObject.layer))
        {
            Attach(collision.gameObject);
        }
    }

    void Attach(GameObject target)
    {
        rb.velocity = Vector2.zero;
        gameObject.transform.parent = target.gameObject.transform;
        attached = true;

    }
    public void Unattach()
    {
        attached = false;   
        StopCoroutine(ApplyGrappleForce());
        Destroy(this.gameObject);
    }
    IEnumerator ApplyGrappleForce()
    {
        while (attached)
        {
            playerController.ApplyForce((transform.position - player.transform.position) * grappleForce);
            yield return null;
        }

    }
}
