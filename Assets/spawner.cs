﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawner : MonoBehaviour
{
    //basic script for infinitely spawning enemies

    public Transform[] spawnPoints;
    public GameObject rangedEnemy, enemy;

    private void Start()
    {
        InvokeRepeating("SpawnEnemy", 2f, 2f);
    }
    
    public void SpawnEnemy()
    {
        int spawnPoint = Random.Range(0, spawnPoints.Length);
        int enemyType = Random.Range(0, 2);
        if(enemyType == 0)
        {
            GameObject spawnedEnemy = Instantiate(rangedEnemy, spawnPoints[spawnPoint].position, spawnPoints[spawnPoint].rotation);
            
        }
        else if (enemyType == 1)
        {
            GameObject spawnedEnemy = Instantiate(enemy, spawnPoints[spawnPoint].position, spawnPoints[spawnPoint].rotation);
        }
    }

}
