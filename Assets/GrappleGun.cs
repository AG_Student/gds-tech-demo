﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrappleGun : RocketLauncher, I_Weapon
{
    public bool firing = false;
    GameObject shot;

    private new void Start()
    {
        base.Start();
    }


    public new void Attack()
    {
        if (!firing)
        {
             
            firing = true;
             
            shot = Instantiate(projectile, shotOrigin.position, Quaternion.identity);
            currentAmmo--;
            ui.UpdateAmmo(currentAmmo, maxAmmo);
        }
        else if (firing)
        {
            if (shot.GetComponent<GrappleHook>().attached)
            {
                shot.GetComponent<GrappleHook>().Unattach();
                firing = false;
            }
            else
            {
                Destroy(shot.gameObject);
                firing = false;
            }
        }
    }
}
