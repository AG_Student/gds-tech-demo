﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    //script for health bar

    Transform bar;
    
    public float maxHP = 1;
    public float currentHP = 1;
    
    // Start is called before the first frame update
    void Start()
    {
        bar = transform.Find("barAnchor");
        
        SetSize(maxHP);
       
        
    }

    private void Update()
    {
        SetSize(currentHP);
    }

    public void SetSize(float HP)
    {
        currentHP = HP;
        if (currentHP >= 0)
        {
            bar.localScale = new Vector3(currentHP / maxHP, 1f);
        }
        else
        {
            bar.localScale = new Vector3(0, 1f);
        }
    
        
    }
    
}
