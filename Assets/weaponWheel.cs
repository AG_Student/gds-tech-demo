﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class weaponWheel : MonoBehaviour
{
    public Image i0;
    public Image i1;
    public Image i2;
    public Image i3;
    public GameObject player;
    public static int number;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.Q))
        {
            i0.enabled = true;
            i1.enabled = true;
            i2.enabled = true;
            i3.enabled = true;
        }
        if (Input.GetKeyUp(KeyCode.Q))
        {
            i0.enabled = false;
            i1.enabled = false;
            i2.enabled = false;
            i3.enabled = false;
            number = mouseHover.num;
            player.GetComponent<WeaponManager>().EnableWeapon(number);
        }
    }

    
}
