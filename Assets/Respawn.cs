﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Respawn : MonoBehaviour
{
    public Vector3 respawnLocation;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        respawnLocation = player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void updateLocation(Vector3 pos)
    {
        respawnLocation = pos;
    }

    public void respawn()
    {
        player.transform.position = respawnLocation;
    }
}
