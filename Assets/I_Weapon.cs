﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface I_Weapon
{
    void Attack();
    int ReturnAmmo();
}
