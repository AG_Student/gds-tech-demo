﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MachineGunBullet : MonoBehaviour
{
    public Rigidbody2D rb;
    public float bulletForce;
    public int damage, targetLayer;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == targetLayer)
        {
            I_Damageable enemy = collision.gameObject.GetComponent<I_Damageable>();
            enemy.Damage(damage);
        }
        Destroy(this.gameObject);
        
    }

    public void SetTrajectory(Vector2 dir)
    {
        dir = dir.normalized;
        rb.AddForce(dir * bulletForce, ForceMode2D.Impulse);
    }
}
