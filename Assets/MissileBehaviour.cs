﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileBehaviour : MonoBehaviour
{
    public Rigidbody2D rb;
    public float force;
    public GameObject mouse;
    public int damage;

    public GameObject explosion;

    // Start is called before the first frame update
    public void Start()
    {
        mouse = GameObject.FindWithTag("Mouse");
        rb = GetComponent<Rigidbody2D>();
        Vector2 dir = mouse.transform.position - transform.position;
        dir = dir.normalized;
        rb.AddForce(dir * force, ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag != "Player")
        {
            if(collision.gameObject.layer == 10)
            {
                collision.gameObject.GetComponent<I_Damageable>().Damage(damage);
            }
            Detonate();
        }
        
    }

    public void Detonate()
    {
        Instantiate(explosion, transform.position, Quaternion.identity);
        Destroy(this.gameObject);
    }

}
